from flask import Flask

app = Flask(__name__)

@app.route('/')
def index():
  return """
  <html>
  <head>
    <script src="https://aframe.io/releases/1.0.4/aframe.min.js"></script>
    <script src="https://unpkg.com/aframe-environment-component/dist/aframe-environment-component.min.js"></script>
  </head>
  <body>
    <a-scene>
		<a-assets>
		    <img id="stones" src=" stone.png" />
		</a-assets>
	   <a-entity environment="preset: osiris"></a-entity>
       <a-box 
		position="-1 2 -5"
		rotation="0 90 0"
		scale="13 3 0.5"
		src="#stones">
	   </a-box>
	   <a-box 
		position="1 2 -5"
		rotation="0 90 0"
		scale="13 3 0.5"
		src="#stones">
		</a-box>
	    <a-box 
		position="1 2 1"
		rotation="0 0 0"
		scale="6 3 0.5"
		src="#stones">
		</a-box>
    </a-scene>
  </body> 
</html>
"""

if __name__ == '__main__':
    app.debug = True
    app.run(host="0.0.0.0")
